// Copyright 2021 Anyalai Richard

#include <stdint.h>
#include <stdio.h>

#define N 5

extern int cat_detector(char *picture, uint8_t n);

char input [N*N] = {
	'^', 'p', '^', 'K', 'K',
	'p', 'w', 'p', 'K', 'K',
	'^', 'k', '^', 'k', '^',
	'k', 'o', 'k', 'W', 'k',
	'k', 'k', 'k', 'k', 'k',
};

int main()
{
	uint8_t cats_detected = cat_detector(input, N);

	printf("Found %d cats\n", cats_detected);

	return 0;
}
