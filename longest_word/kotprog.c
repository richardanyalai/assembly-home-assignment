// Copyright 2021 Anyalai Richard

#include <stdint.h>
#include <stdio.h>

extern int longest_word(char *text);

char *text = "Som3body once told me the world is gonna roll me";

int main()
{
	uint8_t result = longest_word(text);

	printf("The longest word is %d characters long\n", result);

	return 0;
}
