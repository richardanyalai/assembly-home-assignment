// Copyright 2021 Anyalai Richard

#include <stdint.h>
#include <stdio.h>

extern int word_count(char *text);

char *text = "Som3body once told me the world is gonna roll me";

int main()
{
	uint8_t result = word_count(text);

	printf("%s -> %d\n", text, result);

	return 0;
}
