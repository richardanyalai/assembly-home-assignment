// Copyright 2021 Richard Anyalai

.syntax unified

.section .text
.globl word_count

word_count:
	push {r4-r11, lr}

	mov r4, r0			// getting the string parameter
	mov r0, #0			// number of words

loop:
	ldrb r1, [r4]		// getting the current char

	add r4, 1			// increase the index to the next char

	cmp r1, 65			// before uppercase 'A'
	blt end_word		// the currend word ended here

	cmp r1, 122			// after lowercase 'z'
	bgt end_word		// the currend word ended here

	cmp r1, 90			// after uppercase 'Z'
	ble cont_loop		// continue the loop

	cmp r1, 97			// before 'a'
	blt end_word		// the currend word ended here

cont_loop:
	b loop				// loop

end_word:
	add r0, #1			// we found a new word

	cmp r1, 0			// if we reached the end of the string
	beq return			// return the number of words

	b loop				// return to the loop

return:
	pop {r4-r11, pc}
