// Copyright 2021 Richard Anyalai

.intel_syntax noprefix

.section .text
.global word_count

word_count:
	push ebp
	mov ebp, esp

	mov edx, [ebp+8]		// getting the string parameter
	xor eax, eax			// number of words

loop:
	mov cl, byte ptr [edx]	// getting the current char

	inc edx					// increase the index to the next char

	cmp cl, 65				// before uppercase 'A'
	jl end_word				// the currend word ended here

	cmp cl, 122				// after lowercase 'z'
	jg end_word				// the currend word ended here

	cmp cl, 90				// after uppercase 'Z'
	jle cont_loop			// continue the loop

	cmp cl, 97				// before 'a'
	jl end_word				// the currend word ended here

cont_loop:
	jmp loop				// return to the loop

end_word:
	inc eax					// we found a new word

	cmp cl, 0				// if we reached the end of the string
	je return				// return the number of words

	jmp loop				// return to the loop

return:
	mov esp, ebp
	pop ebp
	ret
